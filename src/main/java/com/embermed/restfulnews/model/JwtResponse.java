package com.embermed.restfulnews.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public final class JwtResponse {
	
	private final String token;

	@JsonCreator
	public JwtResponse(@JsonProperty("token") String token) {
		this.token = token;
	}
}
