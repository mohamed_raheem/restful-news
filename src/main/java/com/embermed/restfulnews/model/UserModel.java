package com.embermed.restfulnews.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public final class UserModel {

	private final String email;
	private final String password;
	
	@JsonCreator
	public UserModel(@JsonProperty("email") String email, @JsonProperty("password") String password) {
		this.email = email;
		this.password = password;
	}
	
	@Override
    public boolean equals(Object obj) {
    	if(obj == this)
			return true;
		else if(!(obj instanceof UserModel))
			return false;
		else {
			UserModel that = (UserModel) obj;
			return Objects.equals(this.getEmail(), that.getEmail());
		}
    }
    
    @Override
    public int hashCode() {
    	return Objects.hash(getEmail());
    }

	@Override
	public String toString() {
		return "UserModel [email=" + email + ", password=" + password + "]";
	}
}
