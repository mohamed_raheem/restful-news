package com.embermed.restfulnews.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter @RequiredArgsConstructor
public final class ErrorResponse {

	private final String errorCode;
	private final String errorMessage;
}
