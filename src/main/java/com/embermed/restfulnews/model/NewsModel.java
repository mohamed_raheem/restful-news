package com.embermed.restfulnews.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public final class NewsModel {

	private final String name;
	private final String description;
    private final String iconUrl;

    @JsonCreator
	public NewsModel(@JsonProperty("name") String name, @JsonProperty("description") String description, @JsonProperty("iconUrl") String iconUrl) {
		this.name = name;
		this.description = description;
		this.iconUrl = iconUrl;
	}
    
    @Override
    public boolean equals(Object obj) {
    	if(obj == this)
			return true;
		else if(!(obj instanceof NewsModel))
			return false;
		else {
			NewsModel that = (NewsModel) obj;
			return Objects.equals(this.getName(), that.getName())
					&& Objects.equals(this.getDescription(), that.getDescription())
					&& Objects.equals(this.getIconUrl(), that.getIconUrl());
		}
    }
    
    @Override
    public int hashCode() {
    	return Objects.hash(getName());
    }

	@Override
	public String toString() {
		return "NewsModel [name=" + name + ", description=" + description + ", iconUrl=" + iconUrl + "]";
	}
}
