package com.embermed.restfulnews;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulNewsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfulNewsApplication.class, args);
	}

}
