package com.embermed.restfulnews.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.embermed.restfulnews.model.NewsModel;
import com.embermed.restfulnews.service.NewsService;
import com.embermed.restfulnews.service.UserService;
import com.embermed.restfulnews.service.UserService.Action;

@RestController
public class NewsController {

	private final NewsService newsService;
	private final UserService userService;
	
	@Autowired
	public NewsController(NewsService newsService, UserService userService) {
		this.newsService = newsService;
		this.userService = userService;
	}

	@GetMapping(path="/news", produces = APPLICATION_JSON_VALUE)
	public Map<String, Object> getAllNews(
			@Min(1) @RequestParam(name="pageNum", required = false, defaultValue = "1") int pageNum,
			@Min(1) @RequestParam(name="pageSize", required = false, defaultValue = "3") int pageSize) {
		
		return newsService.getAllNews(pageNum, pageSize);
	}
	
	@GetMapping(path="/user/news", produces = APPLICATION_JSON_VALUE)
	public Map<String, List<NewsModel>> getNewsByEmail() {
		String email = SecurityContextHolder.getContext().getAuthentication().getName();
		return newsService.getUserNews(email);
	}
	
	@PostMapping(path="/news/subscribe", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	public Map<String, Set<NewsModel>> subscribeTo(@NotEmpty @RequestBody Map<String, LinkedHashSet<NewsModel>> requiredNews) {
		String email = SecurityContextHolder.getContext().getAuthentication().getName();
		Set<NewsModel> subscribedTo = userService.manageSubscripedNews(email , requiredNews.get("requiredNews"), Action.SUBSCRIPE);
		return Map.of("newsAdded", subscribedTo);
	}
	
	@DeleteMapping(path="/news/unsubscribe", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	public Map<String, Set<NewsModel>> unsubscribeFrom(@NotEmpty @RequestBody Map<String, LinkedHashSet<NewsModel>> requiredNews) {
		String email = SecurityContextHolder.getContext().getAuthentication().getName();
		Set<NewsModel> subscribedTo = userService.manageSubscripedNews(email , requiredNews.get("requiredNews"), Action.UNSUBSCRIPE);
		return Map.of("newsRemoved", subscribedTo);
	}
}
