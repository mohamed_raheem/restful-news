package com.embermed.restfulnews.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.embermed.restfulnews.model.JwtResponse;
import com.embermed.restfulnews.model.UserModel;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private final AuthenticationManager authenticationManager;
	
	public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
	
		UserModel user;
		try {
			user = new ObjectMapper().readValue(request.getInputStream(), UserModel.class);
		} catch (IOException e) {
			e.printStackTrace();
			throw new BadCredentialsException("Could not deserialize request body!");
		}
		
		UsernamePasswordAuthenticationToken token = 
				new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword());
		Authentication authentication = authenticationManager.authenticate(token);
		return authentication;
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		
		UserDetails user = (UserDetails) authResult.getPrincipal();
		String token = JwtUtil.createToken(user.getUsername());
		String jwtResponse = new ObjectMapper().writeValueAsString(new JwtResponse(token));
		
		response.addHeader("Content-Type", "application/json");
		response.getWriter().write(jwtResponse);
	}
}
