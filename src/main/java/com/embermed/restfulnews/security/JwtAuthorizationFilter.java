package com.embermed.restfulnews.security;

import static com.embermed.restfulnews.security.JwtUtil.HEADER;
import static com.embermed.restfulnews.security.JwtUtil.PREFIX;
import static com.embermed.restfulnews.security.JwtUtil.extractClaim;
import static com.embermed.restfulnews.security.JwtUtil.extractSubject;
import static com.embermed.restfulnews.security.JwtUtil.isValidToken;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

	public JwtAuthorizationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
	
		String header = request.getHeader(HEADER);

		if (header == null || !header.toLowerCase().startsWith(PREFIX)) {
			chain.doFilter(request, response);
			return;
		}

		String token = header.substring(PREFIX.length());
		Authentication authentication = authenticate(token);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(request, response);	
	}
	
	private Authentication authenticate(String token) {
		
		Authentication authentication = null;
		if(isValidToken(token)) {
			String email = extractSubject(token);
			String role = extractClaim(token, "role");
			authentication = 
					new UsernamePasswordAuthenticationToken(email, null, List.of(new SimpleGrantedAuthority(role)));
		}
		
		return authentication;
	}
}
