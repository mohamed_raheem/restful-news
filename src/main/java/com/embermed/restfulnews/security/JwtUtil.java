package com.embermed.restfulnews.security;

import java.util.Date;
import javax.crypto.SecretKey;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;
import io.jsonwebtoken.impl.crypto.MacProvider;

public class JwtUtil {

	private static final String BASE64_SECRET;
	public static final int EXPIRATION = 86400000; // 1 day
	public static final String HEADER = "Authorization";
	public static final String PREFIX = "bearer ";
	
	static {
		SecretKey key = MacProvider.generateKey(SignatureAlgorithm.HS256);
		BASE64_SECRET = TextCodec.BASE64.encode(key.getEncoded());
	}
	
	public static String createToken(String email) {
		
		return Jwts.builder()
				.setSubject(email)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION))
				.claim("role", "USER")
				.signWith(SignatureAlgorithm.HS256, BASE64_SECRET)
				.compact();
	}
	
	public static boolean isValidToken(String jwt) {
		
		try {
			Jwts.parser().setSigningKey(BASE64_SECRET).parseClaimsJws(jwt);
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static String extractSubject(String jwt) {
		return Jwts.parser().setSigningKey(BASE64_SECRET).parseClaimsJws(jwt).getBody().getSubject();
	}
	
	public static String extractClaim(String jwt, String claim) {
		return Jwts.parser().setSigningKey(BASE64_SECRET).parseClaimsJws(jwt).getBody().get(claim, String.class);
	}
}
