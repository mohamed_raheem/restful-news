package com.embermed.restfulnews.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.embermed.restfulnews.model.ErrorResponse;

@ControllerAdvice
public class ExceptionHandlerAdvice {

	@ExceptionHandler(value = {Exception.class})
	public ResponseEntity<ErrorResponse> genericExceptionsHandler(Exception ex) {
		return ResponseEntity
				.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(new ErrorResponse("500", "someThing wrong happened, please try again later!"));
	}
}
