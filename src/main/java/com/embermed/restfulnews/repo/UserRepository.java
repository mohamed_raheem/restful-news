package com.embermed.restfulnews.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.embermed.restfulnews.entity.AppUser;

@Repository
public interface UserRepository extends JpaRepository<AppUser, Long> {
	
	@Query("select u from AppUser u left join fetch u.subscriptions where u.email=:email")
	Optional<AppUser> findUserByEmailInitialized(String email);
	
	Optional<AppUser> findUserByEmail(String email);
}
