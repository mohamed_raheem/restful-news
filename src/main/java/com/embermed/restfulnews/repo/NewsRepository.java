package com.embermed.restfulnews.repo;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.embermed.restfulnews.entity.News;
import com.embermed.restfulnews.model.NewsModel;

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {

	@Query("select new com.embermed.restfulnews.model.NewsModel(n.name, n.description, n.iconUrl) from News n")
	Page<NewsModel> findAllNews(Pageable pageable);
	
	@Query("select new com.embermed.restfulnews.model.NewsModel(n.name, n.description, n.iconUrl) "
			+ " from News n join n.appUsers u where u.email=:email")
	List<NewsModel> findNewsByUserEmail(String email);
	
	Set<News> findByNameIn(Set<String> newsName);
	
}
