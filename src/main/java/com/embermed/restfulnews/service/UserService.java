package com.embermed.restfulnews.service;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.embermed.restfulnews.entity.AppUser;
import com.embermed.restfulnews.entity.News;
import com.embermed.restfulnews.exceptions.InvalidDataException;
import com.embermed.restfulnews.model.NewsModel;
import com.embermed.restfulnews.repo.NewsRepository;
import com.embermed.restfulnews.repo.UserRepository;

@Service
public class UserService {
	
	private final UserRepository userRepository;
	private final NewsRepository newsRepository;
	
	public static enum Action {
		SUBSCRIPE, UNSUBSCRIPE;
	}
	
	public UserService(UserRepository userRepository, NewsRepository newsRepository){
		this.userRepository = userRepository;
		this.newsRepository = newsRepository;
	}
	
	@Transactional
	public Set<NewsModel> manageSubscripedNews(String email, LinkedHashSet<NewsModel> news, Action action) {
		if(email == null || email.isEmpty() || news == null || news.isEmpty())
			throw new IllegalArgumentException();
		
		// keep names in same order
		Set<String> newsNames = new LinkedHashSet<>();
		for(NewsModel newsItem : news) {
			newsNames.add(newsItem.getName());
		}
		
		Optional<AppUser> appUserEntity = userRepository.findUserByEmailInitialized(email);
		Set<News> newsEntities = newsRepository.findByNameIn(newsNames);
		if(newsEntities.size() < news.size())
			throw new InvalidDataException("One or more of the subscription news is not valid: " + news);
		
		Set<News> subscriptionChanges;
		if(action == Action.SUBSCRIPE) {
			subscriptionChanges = appUserEntity.get().addNews(newsEntities);
		} else {
			subscriptionChanges = appUserEntity.get().removeNews(newsEntities);
		}
		
		Set<NewsModel> result = new LinkedHashSet<>();
		for(News n: subscriptionChanges) {
			result.add(new NewsModel(n.getName(), n.getDescription(), n.getIconUrl()));
		}
		
		return result;
	}
}
