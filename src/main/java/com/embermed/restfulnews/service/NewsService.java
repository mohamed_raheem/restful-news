package com.embermed.restfulnews.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.embermed.restfulnews.model.NewsModel;
import com.embermed.restfulnews.repo.NewsRepository;

@Service
public class NewsService {

	private final NewsRepository newsRepository;
	
	@Autowired
	public NewsService(NewsRepository newsRepository){
		this.newsRepository = newsRepository;
	}
	
	@Transactional(readOnly = true)
	public Map<String, Object> getAllNews(int targetPage, int pageSize) {
		
		if(targetPage < 1 || pageSize < 1)
			throw new IllegalArgumentException("page number and page size should be greater than one");
		
		Page<NewsModel> newsPage = 
				newsRepository.findAllNews(PageRequest.of(targetPage-1, pageSize));
		return Map.of(
				"currentPage", newsPage.getNumber()+1,
				"news", newsPage.getContent(),
				"totalItems", newsPage.getTotalElements(),
				"totalPages", newsPage.getTotalPages());
	}
	
	@Transactional(readOnly = true)
	public Map<String, List<NewsModel>> getUserNews(String email) {
		if(email == null || email.isEmpty())
			throw new IllegalArgumentException("user email can not be empty");
		
		List<NewsModel> userNews = newsRepository.findNewsByUserEmail(email);
		return Map.of("news", userNews);
	}
}
