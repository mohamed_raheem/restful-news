package com.embermed.restfulnews.entity;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.NaturalId;

import lombok.Getter;

@Entity(name = "AppUser")
@Table(name = "app_user")
@Getter
public class AppUser {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, unique = true)
	@NaturalId
	private String email;
	
	private String password;
	
	@ManyToMany(fetch = FetchType.LAZY, 
			cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	@JoinTable(name = "user_news", 
		joinColumns = @JoinColumn(name="user_id", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name="news_id", referencedColumnName = "id"))
	private Set<News> subscriptions = new LinkedHashSet<News>();
	
	AppUser() {}
	
	public AppUser(@Email @NotEmpty String email, @NotEmpty String password) {
		this.email = email;
		this.password = password;
	}

	@Version
	private int version;
	
	@Override
	public boolean equals(Object obj) {
		if(obj == this)
			return true;
		else if(!(obj instanceof AppUser))
			return false;
		else {
			AppUser that = (AppUser) obj;
			return Objects.equals(this.getEmail(), that.getEmail());
		}
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(this.getEmail());
	}
	
	public Set<News> addNews(Set<News> newsSet) {
		Set<News> addedNews = new LinkedHashSet<>();
		for(News news : newsSet) {
			if(this.getSubscriptions().add(news)){
				addedNews.add(news);
			}
		}
		return addedNews;
	}
	
	public Set<News> removeNews(Set<News> newsSet) {
		Set<News> removedNews = new LinkedHashSet<>();
		for(News news : newsSet) {
			if(this.getSubscriptions().remove(news)){
				removedNews.add(news);
			}
		}
		return removedNews;
	}
}
