package com.embermed.restfulnews.entity;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.NaturalId;

import lombok.Getter;

@Entity(name = "News")
@Table(name = "news")
@Getter
public class News {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, unique = true)
	@NaturalId
	private String name;
	
	private String description;
	
	@Column(name = "icon_url")
	private String iconUrl;
	
	@Version
	private int version;
	
	@ManyToMany(mappedBy = "subscriptions")
	private Set<AppUser> appUsers = new LinkedHashSet<AppUser>();
	
	News() {}
	
	public News(String name) {
		if(name == null || name.isEmpty())
			throw new IllegalArgumentException("news name can not be null");
		this.name = name;
	}
	
	public News(String name, String description, String iconUrl) {
		this(name);
		this.description = description;
		this.iconUrl = iconUrl;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == this)
			return true;
		else if(!(obj instanceof News))
			return false;
		else {
			News that = (News) obj;
			return Objects.equals(this.getName(), that.getName());
		}
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.getName());
	}
}
