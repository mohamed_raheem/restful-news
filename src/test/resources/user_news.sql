-- populate users table
insert into app_user(id, email, password, version) 
values(1, 'test@email.com', '{bcrypt}$2a$10$zjKx/rWWL4pndIBIfwgQdOAfTbT0rcn/VFxBV21qVWN.Wkpi4OPHm', 0);

-- populate user_news table
insert into user_news(user_id, news_id) values(1, 1);
insert into user_news(user_id, news_id) values(1, 2);
insert into user_news(user_id, news_id) values(1, 3);
insert into user_news(user_id, news_id) values(1, 4);
insert into user_news(user_id, news_id) values(1, 5);