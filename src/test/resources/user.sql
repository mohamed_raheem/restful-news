-- populate users table
insert into app_user(id, email, password, version) values
(1, 'test@email.com', '{bcrypt}$2a$10$zjKx/rWWL4pndIBIfwgQdOAfTbT0rcn/VFxBV21qVWN.Wkpi4OPHm', 0);