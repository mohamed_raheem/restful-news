package com.embermed.restfulnews.repo;

import static org.hamcrest.CoreMatchers.is;

import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

import com.embermed.restfulnews.entity.AppUser;
import com.embermed.restfulnews.entity.News;

import static org.hamcrest.MatcherAssert.assertThat;

@DataJpaTest
class UserRepositoryIntegrationTest {

	@Autowired
	UserRepository userRepository;

	@Test
	@Sql(scripts = { "classpath:news.sql", "classpath:user_news.sql" })
	void givenFindByEmail_whenUserNewsExists_thenReturnNewsListInitialized() {

		// setup
		String email = "test@email.com";

		// test
		Optional<AppUser> user = userRepository.findUserByEmailInitialized(email);

		assertThat(user.isEmpty(), is(false));
		assertThat(user.get().getEmail(), is(email));
		assertThat(user.get().getSubscriptions().size(), is(5));

		News news1 = new News("sport");
		News news2 = new News("politics");
		News news3 = new News("movies");
		News news4 = new News("football");
		News news5 = new News("egypt");
		assertThat(user.get().getSubscriptions(), is(Set.of(news1, news2, news3, news4, news5)));
	}

	@Test
	@Sql(scripts = { "classpath:news.sql", "classpath:user.sql" })
	void givenFindByEmail_whenNoUserNewsExists_thenReturnEmptyNewsList() {
		// setup
		String email = "test@email.com";

		// test
		Optional<AppUser> user = userRepository.findUserByEmailInitialized(email);

		assertThat(user.isEmpty(), is(false));
		assertThat(user.get().getEmail(), is(email));
		assertThat(user.get().getSubscriptions().size(), is(0));

	}
}
