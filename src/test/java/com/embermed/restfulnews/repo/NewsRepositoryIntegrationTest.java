package com.embermed.restfulnews.repo;

import static org.hamcrest.CoreMatchers.is;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.jdbc.Sql;

import com.embermed.restfulnews.entity.News;
import com.embermed.restfulnews.model.NewsModel;

import static org.hamcrest.MatcherAssert.assertThat;

@DataJpaTest
class NewsRepositoryIntegrationTest {

	@Autowired
	NewsRepository newsRepository;

	private static List<NewsModel> expectedNewsList;
	
	@BeforeAll
	static void init() {
		expectedNewsList = List.of(
				new NewsModel("sport", "the sports news", "/news/sports.jpg"),
				new NewsModel("politics", "the politics news", "/news/politics.jpg"),
				new NewsModel("movies", "the movies news", null),
				new NewsModel("football", "the football news", "/default.jpg"),
				new NewsModel("egypt", "egypt news", null),
				new NewsModel("music", "music news", "/news/music.jpg"),
				new NewsModel("tech", "tech news", null),
				new NewsModel("liverpool", "liverpool news", "/news/liverpool.jpg"),
				new NewsModel("java", "java news", "/default.jpg"),
				new NewsModel("apple", "apple news", "/default.jpg"));

	}
	
	@Test
	void givenNoNews_whenQuery_thenReturnEmptyPage() {

		// setup
		int pageNum = 0, pageSize = 5;

		// test
		Page<NewsModel> newsPage = newsRepository.findAllNews(PageRequest.of(pageNum, pageSize));

		// verify
		assertThat(newsPage.getContent().size(), is(0));
		assertThat(newsPage.getTotalElements(), is(0l));
		assertThat(newsPage.getTotalPages(), is(0));
	}

	@Test
	@Sql(scripts = { "classpath:news.sql" })
	void givenNewsExist_whenQueryAllNews_thenReturnSinglePage() {

		// setup
		int pageNum = 0, allContent = 10;

		// test
		Page<NewsModel> page1 = newsRepository.findAllNews(PageRequest.of(pageNum, allContent));

		// verify
		assertThat(page1.getContent().size(), is(allContent));
		assertThat(page1.getTotalElements(), is(Long.valueOf(allContent)));
		assertThat(page1.getTotalPages(), is(1));
		assertThat(page1.getContent(), is(expectedNewsList));
	}

	@Test
	@Sql(scripts = { "classpath:news.sql" })
	void givenNewsExist_whenQuerySomeNews_thenPaginate() {

		// setup
		int pageSize = 3, allContent = 10;

		// test
		Page<NewsModel> page1 = newsRepository.findAllNews(PageRequest.of(0, pageSize));
		Page<NewsModel> page2 = newsRepository.findAllNews(PageRequest.of(1, pageSize));
		Page<NewsModel> page3 = newsRepository.findAllNews(PageRequest.of(2, pageSize));
		Page<NewsModel> page4 = newsRepository.findAllNews(PageRequest.of(3, pageSize));

		// verify page1
		assertThat(page1.getTotalElements(), is(Long.valueOf(allContent)));
		assertThat(page1.getTotalPages(), is(4));
		assertThat(page1.getContent().size(), is(3));
		assertThat(page1.getContent(), is(expectedNewsList.subList(0, 3)));

		// verify page2
		assertThat(page2.getTotalElements(), is(Long.valueOf(allContent)));
		assertThat(page2.getTotalPages(), is(4));
		assertThat(page2.getContent().size(), is(3));
		assertThat(page2.getContent(), is(expectedNewsList.subList(3, 6)));

		// verify page3
		assertThat(page3.getTotalElements(), is(Long.valueOf(allContent)));
		assertThat(page3.getTotalPages(), is(4));
		assertThat(page3.getContent().size(), is(3));
		assertThat(page3.getContent(), is(expectedNewsList.subList(6, 9)));

		// verify page4
		assertThat(page4.getTotalElements(), is(Long.valueOf(allContent)));
		assertThat(page4.getTotalPages(), is(4));
		assertThat(page4.getContent().size(), is(1));
		assertThat(page4.getContent(), is(expectedNewsList.subList(9, 10)));
	}

	@Test
	public void givenFindNewsByUserEmail_whenNoNewsExist_thenReturnEmptyList() {
		
		// test
		List<NewsModel> actualUserNews = newsRepository.findNewsByUserEmail("test@email.com");
		
		// verify
		assertThat(actualUserNews.size(), is(0));
	}
	
	@Test
	@Sql(scripts = { "classpath:news.sql", "classpath:user_news.sql" })
	public void givenFindNewsByUserEmail_whenNewsExist_thenReturnAllNews() {
		
		// test
		List<NewsModel> actualUserNews = newsRepository.findNewsByUserEmail("test@email.com");
		
		// verify
		assertThat(actualUserNews.size(), is(5));
		assertThat(actualUserNews, is(expectedNewsList.subList(0, 5)));
	}
	
	@Test
	@Sql(scripts = { "classpath:news.sql" })
	public void givenFindByNameIn_whenNewsNamesExist_thenReturn() {
		
		// test
		Set<News> actualNews = newsRepository.findByNameIn(Set.of("sport", "politics", "non-existing-news"));
		
		// verify
		assertThat(actualNews.size(), is(2));
		News news1 = new News("sport", "the sports news", "/news/sports.jpg");
		News news2 = new News("politics", "the politics news", "/news/politics.jpg");
		assertThat(actualNews, is(Set.of(news1, news2)));
	}
	
	@Test
	@Sql(scripts = { "classpath:news.sql" })
	public void givenFindByNameIn_whenEmptyNamesList_thenReturnEmptyNewsList() {
		
		// test
		Set<News> actualNews = newsRepository.findByNameIn(Set.of());
		
		// verify
		assertThat(actualNews.size(), is(0));
	}
}
