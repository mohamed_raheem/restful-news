package com.embermed.restfulnews.controller;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@Sql(scripts = { "classpath:user.sql" })
class LoginIntegrationTest {

	@Autowired MockMvc mockMvc;
	
	@Test
	void whenValidLogin_thenReturnToken() throws Exception {
		String requestBody = "{\"email\":\"test@email.com\",\"password\":\"Passw0rd\"}";
		mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(requestBody))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.token", notNullValue()));
	}

	@Test
	void whenInValidLogin_then4xx() throws Exception {
		String requestBody = "{\"email\":\"test@email.com\",\"password\":\"1234\"}";
		mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(requestBody))
		.andExpect(status().is4xxClientError());
	}
}
