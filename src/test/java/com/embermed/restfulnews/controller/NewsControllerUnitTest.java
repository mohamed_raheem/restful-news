package com.embermed.restfulnews.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.embermed.restfulnews.model.NewsModel;
import com.embermed.restfulnews.service.NewsService;
import com.embermed.restfulnews.service.UserService;
import com.embermed.restfulnews.service.UserService.Action;

@ExtendWith(MockitoExtension.class)
class NewsControllerUnitTest {

	@Mock NewsService newsServcie;
	@Mock UserService userServcie;
	@InjectMocks NewsController newsController;
	MockMvc mocMvc;
	
	@BeforeEach
	void init() {
		this.mocMvc = MockMvcBuilders.standaloneSetup(newsController).build();
	}
	
	void authenticate() {
		Authentication authentication = Mockito.mock(Authentication.class);
		SecurityContext securityContext = Mockito.mock(SecurityContext.class);
		Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
		Mockito.when(authentication.getName()).thenReturn("test@email.com");
		SecurityContextHolder.setContext(securityContext);
	}
	
	@Test
	void givenGetAllNews_whenNoData_thenReturnEmptyNewsField() throws Exception {
		
		// setup
		int pageSize = 3, pageNum = 1;
		Map<String, Object> newsMap = Map.of(
				"currentPage", 1,
				"news", List.of(),
				"totalItems", 0,
				"totalPages", 0);
		
		Mockito.when(newsServcie.getAllNews(pageNum, pageSize)).thenReturn(newsMap);
		
		// test
		mocMvc.perform(get("/news"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.currentPage", is(1)))
		.andExpect(jsonPath("$.news", is(List.of())))
		.andExpect(jsonPath("$.totalItems", is(0)))
		.andExpect(jsonPath("$.totalPages", is(0)));
	}
	
	@Test
	void givenGetAllNews_whenDataExists_thenReturnNewsList() throws Exception {
		
		// setup
		int pageSize = 3, pageNum = 1;
		List<NewsModel> newList = List.of(
				new NewsModel("sport", "the sports news", "/news/sports.jpg"),
				new NewsModel("politics", "the politics news", "/news/politics.jpg"),
				new NewsModel("movies", "the movies news", null));
		Map<String, Object> page1 = Map.of(
				"currentPage", 1,
				"news", newList,
				"totalItems", 10,
				"totalPages", 4);
		
		Mockito.when(newsServcie.getAllNews(pageNum, pageSize)).thenReturn(page1);
		
		// test
		mocMvc.perform(get("/news"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.currentPage", is(1)))
		.andExpect(jsonPath("$.news[0].name", is(newList.get(0).getName())))
		.andExpect(jsonPath("$.news[0].description", is(newList.get(0).getDescription())))
		.andExpect(jsonPath("$.news[0].iconUrl", is(newList.get(0).getIconUrl())))
		.andExpect(jsonPath("$.news[1].name", is(newList.get(1).getName())))
		.andExpect(jsonPath("$.news[1].description", is(newList.get(1).getDescription())))
		.andExpect(jsonPath("$.news[1].iconUrl", is(newList.get(1).getIconUrl())))
		.andExpect(jsonPath("$.news[2].name", is(newList.get(2).getName())))
		.andExpect(jsonPath("$.news[2].description", is(newList.get(2).getDescription())))
		.andExpect(jsonPath("$.news[2].iconUrl", is(newList.get(2).getIconUrl())))
		.andExpect(jsonPath("$.totalItems", is(10)))
		.andExpect(jsonPath("$.totalPages", is(4)));
	}
	
	@Test
	void givenGetNewsByEmail_whenNonExistingEmail_thenReturnEmptyNews() throws Exception {
		
		// setup
		authenticate();
		List<NewsModel> expectedNews = List.of();
		
		String email = "test@email.com";
		Mockito.when(newsServcie.getUserNews(email)).thenReturn(Map.of("news", expectedNews));

		// test
		mocMvc.perform(get("/user/news"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.news.*", hasSize(0)));
	}
	
	@Test
	void givenGetNewsByEmail_whenExistingEmail_thenReturnAllNews() throws Exception {
		
		// setup
		authenticate();
		List<NewsModel> expectedNews = List.of(
				new NewsModel("sport", "the sports news", "/news/sports.jpg"),
				new NewsModel("politics", "the politics news", "/news/politics.jpg"),
				new NewsModel("movies", "the movies news", null));
		
		String email = "test@email.com";
		Mockito.when(newsServcie.getUserNews(email)).thenReturn(Map.of("news", expectedNews));

		// test
		mocMvc.perform(get("/user/news"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.news.*", hasSize(3)))
		.andExpect(jsonPath("$.news[0].name", is(expectedNews.get(0).getName())))
		.andExpect(jsonPath("$.news[0].description", is(expectedNews.get(0).getDescription())))
		.andExpect(jsonPath("$.news[0].iconUrl", is(expectedNews.get(0).getIconUrl())))
		.andExpect(jsonPath("$.news[1].name", is(expectedNews.get(1).getName())))
		.andExpect(jsonPath("$.news[1].description", is(expectedNews.get(1).getDescription())))
		.andExpect(jsonPath("$.news[1].iconUrl", is(expectedNews.get(1).getIconUrl())))
		.andExpect(jsonPath("$.news[2].name", is(expectedNews.get(2).getName())))
		.andExpect(jsonPath("$.news[2].description", is(expectedNews.get(2).getDescription())))
		.andExpect(jsonPath("$.news[2].iconUrl", is(expectedNews.get(2).getIconUrl())));
	}
	
	@Test
	void givenSubscribeTo_whenSubscribedToNews_thenReturnInNewsAddedKey() throws Exception {
		
		// setup
		authenticate();
		
		String requestBody = "{\"requiredNews\":[{\"name\":\"sport\",\"description\":\"the sports news\",\"iconUrl\":\"/news/sports.jpg\"},{\"name\":\"politics\",\"description\":\"the politics news\",\"iconUrl\":\"/news/politics.jpg\"}]}";
		NewsModel model1 = new NewsModel("sport", "the sports news", "/news/sports.jpg");
		NewsModel model2 = new NewsModel("politics", "the politics news", "/news/politics.jpg");
		LinkedHashSet<NewsModel> newsToAdd = new LinkedHashSet<>();
		newsToAdd.add(model1);
		newsToAdd.add(model2);
		
		Mockito.when(userServcie.manageSubscripedNews("test@email.com", newsToAdd, Action.SUBSCRIPE)).thenReturn(newsToAdd);
		
		mocMvc.perform(post("/news/subscribe").contentType(MediaType.APPLICATION_JSON).content(requestBody))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.newsAdded.*", hasSize(2)))
		.andExpect(jsonPath("$.newsAdded[0].name", is(model1.getName())))
		.andExpect(jsonPath("$.newsAdded[0].description", is(model1.getDescription())))
		.andExpect(jsonPath("$.newsAdded[0].iconUrl", is(model1.getIconUrl())))
		.andExpect(jsonPath("$.newsAdded[1].name", is(model2.getName())))
		.andExpect(jsonPath("$.newsAdded[1].description", is(model2.getDescription())))
		.andExpect(jsonPath("$.newsAdded[1].iconUrl", is(model2.getIconUrl())));
	}
	
	@Test
	void givenUnsubscribeFrom_whenUnSubscribedFromNews_thenReturnInNewsAddedKey() throws Exception {
		
		// setup
		authenticate();
		String requestBody = "{\"requiredNews\":[{\"name\":\"sport\",\"description\":\"the sports news\",\"iconUrl\":\"/news/sports.jpg\"},{\"name\":\"politics\",\"description\":\"the politics news\",\"iconUrl\":\"/news/politics.jpg\"}]}";
		NewsModel model1 = new NewsModel("sport", "the sports news", "/news/sports.jpg");
		NewsModel model2 = new NewsModel("politics", "the politics news", "/news/politics.jpg");
		LinkedHashSet<NewsModel> newsToRemove = new LinkedHashSet<>();
		newsToRemove.add(model1);
		newsToRemove.add(model2);
		
		Mockito.when(userServcie.manageSubscripedNews("test@email.com", newsToRemove, Action.UNSUBSCRIPE)).thenReturn(newsToRemove);
		
		mocMvc.perform(delete("/news/unsubscribe").contentType(MediaType.APPLICATION_JSON).content(requestBody))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.newsRemoved.*", hasSize(2)))
		.andExpect(jsonPath("$.newsRemoved[0].name", is(model1.getName())))
		.andExpect(jsonPath("$.newsRemoved[0].description", is(model1.getDescription())))
		.andExpect(jsonPath("$.newsRemoved[0].iconUrl", is(model1.getIconUrl())))
		.andExpect(jsonPath("$.newsRemoved[1].name", is(model2.getName())))
		.andExpect(jsonPath("$.newsRemoved[1].description", is(model2.getDescription())))
		.andExpect(jsonPath("$.newsRemoved[1].iconUrl", is(model2.getIconUrl())));
	}
}
