package com.embermed.restfulnews.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import com.embermed.restfulnews.model.NewsModel;
import com.embermed.restfulnews.security.JwtUtil;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@Sql(scripts = { "classpath:news.sql", "classpath:user.sql" })
class NewsControllerIntegrationTest {

	static List<NewsModel> expectedNewsList;

	@Autowired MockMvc mockMvc;
	
	String token = JwtUtil.createToken("test@email.com");
	
	@Test
	void whenAuthorizationTokenAbsent_theRejectRequest() throws Exception {
		mockMvc.perform(get("/news")).andExpect(status().is4xxClientError());
		mockMvc.perform(get("/user/news")).andExpect(status().is4xxClientError());
		mockMvc.perform(post("/news/subscribe")).andExpect(status().is4xxClientError());
		mockMvc.perform(delete("/news/unsubscribe")).andExpect(status().is4xxClientError());
	}
	
	@Test
	void givenGetAllNews_whenNoParams_thenUseDefaults() throws Exception {
		mockMvc.perform(get("/news").header("Authorization", "Bearer " + token))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.currentPage", is(1)))
		.andExpect(jsonPath("$.news.*", hasSize(3)))
		.andExpect(jsonPath("$.totalItems", is(10)))
		.andExpect(jsonPath("$.totalPages", is(4)))
		.andExpect(content().json("{\"currentPage\":1,\"news\":[{\"name\":\"sport\",\"description\":\"the sports news\",\"iconUrl\":\"/news/sports.jpg\"},{\"name\":\"politics\",\"description\":\"the politics news\",\"iconUrl\":\"/news/politics.jpg\"},{\"name\":\"movies\",\"description\":\"the movies news\",\"iconUrl\":null}],\"totalItems\":10,\"totalPages\":4}"));
	}

	@Test
	void givenGetAllNews_whenInvalidParamValue_thenReturnErrorResponse() throws Exception {
		mockMvc.perform(get("/news").header("Authorization", "Bearer " + token).param("pageNum", "0"))
		.andExpect(status().is5xxServerError())
		.andExpect(jsonPath("$.errorCode", is("500")))
		.andExpect(jsonPath("$.errorMessage", is("someThing wrong happened, please try again later!")));
	}
	
	@Test
	void givenGetAllNews_whenParamsExist_thenPaginateWithParams() throws Exception {
		mockMvc.perform(get("/news").header("Authorization", "Bearer " + token).param("pageNum", "1").param("pageSize", "5"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.currentPage", is(1)))
		.andExpect(jsonPath("$.news.*", hasSize(5)))
		.andExpect(jsonPath("$.totalItems", is(10)))
		.andExpect(jsonPath("$.totalPages", is(2)))
		.andExpect(content().json("{\"currentPage\":1,\"news\":[{\"name\":\"sport\",\"description\":\"the sports news\",\"iconUrl\":\"/news/sports.jpg\"},{\"name\":\"politics\",\"description\":\"the politics news\",\"iconUrl\":\"/news/politics.jpg\"},{\"name\":\"movies\",\"description\":\"the movies news\",\"iconUrl\":null},{\"name\":\"football\",\"description\":\"the football news\",\"iconUrl\":\"/default.jpg\"},{\"name\":\"egypt\",\"description\":\"egypt news\",\"iconUrl\":null}],\"totalItems\":10,\"totalPages\":2}"));
	}
	
	@Test
	@Sql(scripts = { "classpath:news.sql", "classpath:user_news.sql" })
	void givenGetNewsByEmail_whenExistingEmail_thenReturnAllNews() throws Exception {
		mockMvc.perform(get("/user/news").header("Authorization", "Bearer " + token))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.news.*", hasSize(5)))
		.andExpect(content().json("{\"news\":[{\"name\":\"sport\",\"description\":\"the sports news\",\"iconUrl\":\"/news/sports.jpg\"},{\"name\":\"politics\",\"description\":\"the politics news\",\"iconUrl\":\"/news/politics.jpg\"},{\"name\":\"movies\",\"description\":\"the movies news\",\"iconUrl\":null},{\"name\":\"football\",\"description\":\"the football news\",\"iconUrl\":\"/default.jpg\"},{\"name\":\"egypt\",\"description\":\"egypt news\",\"iconUrl\":null}]}"));
	}

	@Test
	void givenSubscribeTo_whenEmptyBody_thenThrow() throws Exception {
		String requestBody = "{}";
		mockMvc.perform(post("/news/subscribe").header("Authorization", "Bearer " + token).contentType(MediaType.APPLICATION_JSON).content(requestBody))
		.andExpect(status().is5xxServerError())
		.andExpect(jsonPath("$.errorCode", is("500")))
		.andExpect(jsonPath("$.errorMessage", is("someThing wrong happened, please try again later!")));
	}

	@Test
	@Sql(scripts = { "classpath:news.sql", "classpath:user_news.sql" })
	void givenSubscribeTo_whenSubscribedToNews_thenReturnInNewsAddedKey() throws Exception {
		String requestBody = "{\"requiredNews\":[{\"name\":\"egypt\",\"description\":\"egypt news\",\"iconUrl\":null},{\"name\":\"music\",\"description\":\"music news\",\"iconUrl\":\"/news/music.jpg\"}]}";
			
		mockMvc.perform(post("/news/subscribe").header("Authorization", "Bearer " + token).contentType(MediaType.APPLICATION_JSON).content(requestBody))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.newsAdded.*", hasSize(1)))
		.andExpect(content().json("{\"newsAdded\":[{\"name\":\"music\",\"description\":\"music news\",\"iconUrl\":\"/news/music.jpg\"}]}"));
	}
	
	@Test
	@Sql(scripts = { "classpath:news.sql", "classpath:user_news.sql" })
	void givenUnsubscribeFrom_whenUnsubscribedFromNews_thenReturnInNewsAddedKey() throws Exception {
		String requestBody = "{\"requiredNews\":[{\"name\":\"egypt\",\"description\":\"egypt news\",\"iconUrl\":null},{\"name\":\"music\",\"description\":\"music news\",\"iconUrl\":\"/news/music.jpg\"}]}";
			
		mockMvc.perform(delete("/news/unsubscribe").header("Authorization", "Bearer " + token).contentType(MediaType.APPLICATION_JSON).content(requestBody))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.newsRemoved.*", hasSize(1)))
		.andExpect(content().json("{\"newsRemoved\":[{\"name\":\"egypt\",\"description\":\"egypt news\",\"iconUrl\":null}]}"));
	}
	
	@Test
	@Sql(scripts = { "classpath:news.sql", "classpath:user.sql" })
	void givenUnsubscribeFrom_whenNosubscribedNews_thenReturnEmptyList() throws Exception {
		String requestBody = "{\"requiredNews\":[{\"name\":\"egypt\",\"description\":\"egypt news\",\"iconUrl\":null},{\"name\":\"music\",\"description\":\"music news\",\"iconUrl\":\"/news/music.jpg\"}]}";
			
		mockMvc.perform(delete("/news/unsubscribe").header("Authorization", "Bearer " + token).contentType(MediaType.APPLICATION_JSON).content(requestBody))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.newsRemoved.*", hasSize(0)));
	}
}
