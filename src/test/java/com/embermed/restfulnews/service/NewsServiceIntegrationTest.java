package com.embermed.restfulnews.service;

import static org.hamcrest.CoreMatchers.is;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.jdbc.Sql;

import com.embermed.restfulnews.model.NewsModel;
import com.embermed.restfulnews.service.NewsService;

import static org.hamcrest.MatcherAssert.assertThat;

@DataJpaTest
@ComponentScan(basePackageClasses = NewsService.class)
class NewsServiceIntegrationTest {
	
	@Autowired
	private NewsService newsService;
	
	@Test
	void givenGetAllNews_whenNoNewsExist_thenReturnNoContent() {
		
		// setup
		int pageNum = 10, pageSize = 3;
		List<NewsModel> expectedNews = List.of();
		
		// test
		Map<String, Object> actualNews = newsService.getAllNews(pageNum, pageSize);
		
		// verify
		assertThat(actualNews.get("news"), is(expectedNews));
		assertThat(actualNews.get("currentPage"), is(10));
		assertThat(actualNews.get("totalItems"), is(0l));
		assertThat(actualNews.get("totalPages"), is(0));
	}
	
	@Test
	@Sql(scripts = { "classpath:news.sql" })
	void givenGetAllNews_whenUsingWrongPageNum_thenReturnNoContent() {
		
		// setup
		int pageNum = 10, pageSize = 3;
		List<NewsModel> expectedNews = List.of();
		
		// test
		Map<String, Object> actualNews = newsService.getAllNews(pageNum, pageSize);
		
		// verify
		assertThat(actualNews.get("news"), is(expectedNews));
		assertThat(actualNews.get("currentPage"), is(10));
		assertThat(actualNews.get("totalItems"), is(10l));
		assertThat(actualNews.get("totalPages"), is(4));
	}
	
	@Test
	@Sql(scripts = { "classpath:news.sql" })
	void givenGetAllNews_whenPageRetrieved_thenReturnInMap() {
		
		// setup
		int pageNum = 2, pageSize = 3;
		List<NewsModel> expectedNews = List.of(
				new NewsModel("football", "the football news", "/default.jpg"),
				new NewsModel("egypt", "egypt news", null),
				new NewsModel("music", "music news", "/news/music.jpg"));
		
		// test
		Map<String, Object> actualNews = newsService.getAllNews(pageNum, pageSize);
		
		// verify
		assertThat(actualNews.get("news"), is(expectedNews));
		assertThat(actualNews.get("currentPage"), is(2));
		assertThat(actualNews.get("totalItems"), is(10l));
		assertThat(actualNews.get("totalPages"), is(4));
	}
	
	@Test
	@Sql(scripts = { "classpath:news.sql", "classpath:user_news.sql" })
	void givenGetUserNews_whenEmailExists_thenReturnAllNews() {
		
		// setup
		String email = "test@email.com";
		List<NewsModel> expectedNews = List.of(
				new NewsModel("sport", "the sports news", "/news/sports.jpg"),
				new NewsModel("politics", "the politics news", "/news/politics.jpg"),
				new NewsModel("movies", "the movies news", null),
				new NewsModel("football", "the football news", "/default.jpg"),
				new NewsModel("egypt", "egypt news", null));
		
		// test
		Map<String, List<NewsModel>> userNews = newsService.getUserNews(email);
		
		// verify
		assertThat(userNews.get("news"), is(expectedNews));
	}
}
