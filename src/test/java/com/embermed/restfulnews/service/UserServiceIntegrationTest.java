package com.embermed.restfulnews.service;

import static org.hamcrest.CoreMatchers.is;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.jdbc.Sql;

import com.embermed.restfulnews.model.NewsModel;
import com.embermed.restfulnews.service.UserService.Action;

import static org.hamcrest.MatcherAssert.assertThat;

@DataJpaTest
@ComponentScan(basePackageClasses = UserService.class)
class UserServiceIntegrationTest {
	
	@Autowired
	private UserService userService;
	
	@Autowired EntityManager em;
	
	@Test
	@Sql(scripts = { "classpath:news.sql", "classpath:user.sql"})
	void givenSubscribe_whenUniqueNewsNotAssigned_thenAddAll() {
		
		// setup
		String email = "test@email.com";
		NewsModel newsModel1 = new NewsModel("music", "music news", "/news/music.jpg");
		NewsModel newsModel2 = new NewsModel("tech", "tech news", null);
		LinkedHashSet<NewsModel> newsToAdd = new LinkedHashSet<>();
		newsToAdd.add(newsModel1);
		newsToAdd.add(newsModel2);

		Set<NewsModel> currentSubscriptions = userService.manageSubscripedNews(email, newsToAdd, Action.SUBSCRIPE);
		em.flush();
		
		assertThat(currentSubscriptions.size(), is(2));
		assertThat(currentSubscriptions, is(newsToAdd));
	}
	
	@Test
	@Sql(scripts = { "classpath:news.sql", "classpath:user_news.sql" })
	void givenSubscribe_whenOneOrMoreNewsAlreadyAssigned_thenAddNonAssigned() {

		// setup
		String email = "test@email.com";
		NewsModel newsModel1 = new NewsModel("football", "the football news", "/default.jpg");
		NewsModel newsModel2 = new NewsModel("tech", "tech news", null);
		LinkedHashSet<NewsModel> newsToAdd = new LinkedHashSet<>();
		newsToAdd.add(newsModel1);
		newsToAdd.add(newsModel2);
		
		Set<NewsModel> currentSubscriptions = userService.manageSubscripedNews(email, newsToAdd, Action.SUBSCRIPE);
		em.flush();

		assertThat(currentSubscriptions.size(), is(1));
		assertThat(currentSubscriptions, is(Set.of(newsModel2)));
	}
	
	@Test
	@Sql(scripts = { "classpath:news.sql", "classpath:user_news.sql" })
	public void givenFindByNameIn_whenSubscribedToAllNews_thenReturnEmptyNewsList() {
		
		// setup
		String email = "test@email.com";
		NewsModel newsModel1 = new NewsModel("sport", "the sports news", "/news/sports.jpg");
		NewsModel newsModel2 = new NewsModel("politics", "the politics news", "/news/politics.jpg");
		LinkedHashSet<NewsModel> newsToAdd = new LinkedHashSet<>();
		newsToAdd.add(newsModel1);
		newsToAdd.add(newsModel2);
		
		Set<NewsModel> currentSubscriptions = userService.manageSubscripedNews(email, newsToAdd, Action.SUBSCRIPE);
		em.flush();

		assertThat(currentSubscriptions.size(), is(0));
	}
	
	@Test
	@Sql(scripts = { "classpath:news.sql", "classpath:user_news.sql" })
	void givenUnsubscribe_whenRemovingNews_thenRemoveSubscribedNewsOnly() {
		
		// setup
		String email = "test@email.com";
		NewsModel newsModel1 = new NewsModel("football", "the football news", "/default.jpg");
		NewsModel newsModel2 = new NewsModel("tech", "tech news", null);
		LinkedHashSet<NewsModel> newsToRemove = new LinkedHashSet<>();
		newsToRemove.add(newsModel1);
		newsToRemove.add(newsModel2);
		
		Set<NewsModel> currentSubscriptions = userService.manageSubscripedNews(email, newsToRemove, Action.UNSUBSCRIPE);
		em.flush();

		assertThat(currentSubscriptions.size(), is(1));
		assertThat(currentSubscriptions, is(Set.of(newsModel1)));

	}
}
