package com.embermed.restfulnews.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.embermed.restfulnews.entity.AppUser;
import com.embermed.restfulnews.entity.News;
import com.embermed.restfulnews.exceptions.InvalidDataException;
import com.embermed.restfulnews.model.NewsModel;
import com.embermed.restfulnews.repo.NewsRepository;
import com.embermed.restfulnews.repo.UserRepository;
import com.embermed.restfulnews.service.UserService.Action;

import static org.hamcrest.MatcherAssert.assertThat;


@ExtendWith(MockitoExtension.class)
public class UserServiceUnitTest {

	@Mock UserRepository userRepository;
	@Mock NewsRepository newsRepository;
	@InjectMocks UserService userService;
	Optional<AppUser> user = Optional.of(new AppUser("test@email.com", "1234"));
	
	@Test
	void givenSubscribe_whenInvalidEmailOrNews_thenThrow() {
		assertThrows(IllegalArgumentException.class, () -> userService.manageSubscripedNews(null, new LinkedHashSet<NewsModel>(), Action.SUBSCRIPE));
		assertThrows(IllegalArgumentException.class, () -> userService.manageSubscripedNews("test@email.com", new LinkedHashSet<NewsModel>(), Action.SUBSCRIPE));
	}
	
	@Test
	void givenSubscribe_whenUniqueNewsNotAssigned_thenAddAll() {
		
		// setup
		String email = "test@email.com";
		NewsModel model1 = new NewsModel("apple", "apple news", "/default.jpg");
		NewsModel model2 = new NewsModel("liverpool", "liverpool news", "/news/liverpool.jpg");
		LinkedHashSet<NewsModel> requiredNewsList = new LinkedHashSet<>();
		requiredNewsList.add(model1);
		requiredNewsList.add(model2);

		// current user subscriptions
		user.get().getSubscriptions().add(new News("sport", "the sports news", "/news/sports.jpg"));
		user.get().getSubscriptions().add(new News("politics", "the politics news", "/news/politics.jpg"));
		Mockito.when(userRepository.findUserByEmailInitialized(email)).thenReturn(user);
		
		Set<News> fetchedEntities = Set.of(new News("apple", "apple news", "/default.jpg"), new News("liverpool", "liverpool news", "/news/liverpool.jpg"));
		Mockito.when(newsRepository.findByNameIn(Set.of("apple", "liverpool"))).thenReturn(fetchedEntities);
		
		Set<NewsModel> addedNews = userService.manageSubscripedNews(email, requiredNewsList, Action.SUBSCRIPE);
		
		assertThat(addedNews.size(), is(2));
		assertThat(addedNews, is(requiredNewsList));
	}
	
	@Test
	void givenSubscribe_whenOneOrMoreNewsAlreadyAssigned_thenAddNonAssigned() {
		// setup
		String email = "test@email.com";
		NewsModel alreadyAssignedModel = new NewsModel("apple", "apple news", "/default.jpg");
		NewsModel newModel = new NewsModel("liverpool", "liverpool news", "/news/liverpool.jpg");
		LinkedHashSet<NewsModel> requiredNewsList = new LinkedHashSet<>();
		requiredNewsList.add(alreadyAssignedModel);
		requiredNewsList.add(newModel);

		// current user subscriptions
		user.get().getSubscriptions().add(new News("sport", "the sports news", "/news/sports.jpg"));
		user.get().getSubscriptions().add(new News("politics", "the politics news", "/news/politics.jpg"));
		user.get().getSubscriptions().add(new News("apple", "apple news", "/default.jpg"));
		Mockito.when(userRepository.findUserByEmailInitialized(email)).thenReturn(user);

		LinkedHashSet<News> fetchedEntities = new LinkedHashSet<>(Set.of(new News("apple", "apple news", "/default.jpg"),
				new News("liverpool", "liverpool news", "/news/liverpool.jpg")));
		Mockito.when(newsRepository.findByNameIn(Set.of("apple", "liverpool"))).thenReturn(fetchedEntities);

		Set<NewsModel> addedNews = userService.manageSubscripedNews(email, requiredNewsList, Action.SUBSCRIPE);

		assertThat(addedNews.size(), is(1));
		assertThat(addedNews, is(Set.of(newModel)));
	}

	@Test
	void givenManageSubscripedNews_whenOneOrMoreNewsNotExist_thenThrowInvalidDataException() {
		
		// setup
		String email = "test@email.com";
		
		NewsModel invalidNewsModel = new NewsModel("fashion", "fashion news", "/default.jpg");
		NewsModel validNewsModel = new NewsModel("liverpool", "liverpool news", "/news/liverpool.jpg");
		LinkedHashSet<NewsModel> requiredNewsList = new LinkedHashSet<>();
		requiredNewsList.add(invalidNewsModel);
		requiredNewsList.add(validNewsModel);

		LinkedHashSet<News> fetchedEntities = new LinkedHashSet<>(Set.of(new News("liverpool", "liverpool news", "/news/liverpool.jpg")));
		Mockito.when(newsRepository.findByNameIn(Set.of("fashion", "liverpool"))).thenReturn(fetchedEntities);
		
		// verify
		assertThrows(InvalidDataException.class, () -> userService.manageSubscripedNews(email, requiredNewsList, Action.SUBSCRIPE));

	}
	
	@Test
	void givenUnsubscribe_whenRemovingNews_thenRemoveSubscribedNewsOnly() {
		
		// setup
		String email = "test@email.com";
		NewsModel assignedModel = new NewsModel("apple", "apple news", "/default.jpg");
		NewsModel unassignedModel = new NewsModel("liverpool", "liverpool news", "/news/liverpool.jpg");
		LinkedHashSet<NewsModel> requiredNewsList = new LinkedHashSet<>();
		requiredNewsList.add(assignedModel);
		requiredNewsList.add(unassignedModel);

		// current user subscriptions
		user.get().getSubscriptions().add(new News("sport", "the sports news", "/news/sports.jpg"));
		user.get().getSubscriptions().add(new News("politics", "the politics news", "/news/politics.jpg"));
		user.get().getSubscriptions().add(new News("apple", "apple news", "/default.jpg"));
		Mockito.when(userRepository.findUserByEmailInitialized(email)).thenReturn(user);

		LinkedHashSet<News> fetchedEntities = new LinkedHashSet<>(Set.of(new News("apple", "apple news", "/default.jpg"),
				new News("liverpool", "liverpool news", "/news/liverpool.jpg")));
		Mockito.when(newsRepository.findByNameIn(Set.of("apple", "liverpool"))).thenReturn(fetchedEntities);

		Set<NewsModel> addedNews = userService.manageSubscripedNews(email, requiredNewsList, Action.UNSUBSCRIPE);

		assertThat(addedNews.size(), is(1));
		assertThat(addedNews, is(Set.of(assignedModel)));
	}
}
