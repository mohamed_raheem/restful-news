package com.embermed.restfulnews.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.jdbc.Sql;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import com.embermed.restfulnews.security.AppUserDetailsService;

@DataJpaTest
@ComponentScan(basePackageClasses = AppUserDetailsService.class)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class UserDetailsServiceIntegrationTest {

	@Autowired AppUserDetailsService userDetailsService;
	
	@Test
	void givenLoadUserByUsername_whenUserNotExists_thenThrow(){
		
		String email = "test@email.com";
		Assertions.assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername(email));
	}
	
	@Test
	@Sql(scripts = { "classpath:user.sql"})
	void givenLoadUserByUsername_whenUserExists_thenRetuenUserDetails(){
		
		String email = "test@email.com";
		UserDetails userDetails = userDetailsService.loadUserByUsername(email);
		assertThat(userDetails, notNullValue());
		assertThat(userDetails.getUsername(), is(email));
		assertThat(userDetails.getPassword(), is("{bcrypt}$2a$10$zjKx/rWWL4pndIBIfwgQdOAfTbT0rcn/VFxBV21qVWN.Wkpi4OPHm"));
	}
}
