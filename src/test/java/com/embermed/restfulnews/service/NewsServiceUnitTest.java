package com.embermed.restfulnews.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import com.embermed.restfulnews.model.NewsModel;
import com.embermed.restfulnews.repo.NewsRepository;
import com.embermed.restfulnews.service.NewsService;

import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
class NewsServiceUnitTest {

	@Mock 
	private NewsRepository newsRepo;
	
	@InjectMocks 
	private NewsService newsService;
	
	@Test
	void givenGetAllNews_whenInvalidArgs_thenThrow() {
		assertThrows(IllegalArgumentException.class, () -> newsService.getAllNews(0, 0));
	}
	
	@Test
	void givenGetAllNews_whenPageRetrieved_thenReturnInMap() {
		
		// setup
		List<NewsModel> expectedNews = List.of(
				new NewsModel("sport", "the sports news", "/news/sports.jpg"),
				new NewsModel("politics", "the politics news", "/news/politics.jpg"),
				new NewsModel("movies", "the movies news", null));
		
		int pageNum = 1, pageSize = 3, totalElements = 10;
		PageRequest pageRequest = PageRequest.of(pageNum-1, pageSize);
		Page<NewsModel> page = new PageImpl<>(expectedNews, pageRequest, totalElements);
		Mockito.when(newsRepo.findAllNews(pageRequest)).thenReturn(page);
		
		// test
		Map<String, Object> actualNews = newsService.getAllNews(pageNum, pageSize);
		
		// verify
		assertThat(actualNews.get("news"), is(expectedNews));
		assertThat(actualNews.get("currentPage"), is(1));
		assertThat(actualNews.get("totalItems"), is(10l));
		assertThat(actualNews.get("totalPages"), is(4));
	}
	
	@Test
	void givenGetUserNews_whenInvalidEmail_thenThrow() {
		assertThrows(IllegalArgumentException.class, () -> newsService.getUserNews(""));
	}
	
	@Test
	void givenGetUserNews_whenEmailExists_thenReturnAllNews() {
		
		// setup
		String email = "test@email.com";
		List<NewsModel> expectedNews = List.of(
				new NewsModel("sport", "the sports news", "/news/sports.jpg"),
				new NewsModel("politics", "the politics news", "/news/politics.jpg"),
				new NewsModel("movies", "the movies news", null));
		Mockito.when(newsRepo.findNewsByUserEmail(email)).thenReturn(expectedNews);
		
		// test
		Map<String, List<NewsModel>> userNews = newsService.getUserNews(email);
		
		// verify
		assertThat(userNews.get("news"), is(expectedNews));
	}
	
}
