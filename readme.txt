After running the application, execute the following DML statements (Hibernate will take care of tables creation):

insert into news(id, name, description, icon_url, version) values(1, 'sport', 'the sports news', '/news/sports.jpg', 0);
insert into news(id, name, description, icon_url, version) values(2, 'politics', 'the politics news', '/news/politics.jpg', 0);
insert into news(id, name, description, icon_url, version) values(3, 'movies', 'the movies news', null, 0);
insert into news(id, name, description, icon_url, version) values(4, 'football', 'the football news', '/default.jpg', 0);
insert into news(id, name, description, icon_url, version) values(5, 'egypt', 'egypt news', null, 0);
insert into news(id, name, description, icon_url, version) values(6, 'music', 'music news', '/news/music.jpg', 0);
insert into news(id, name, description, icon_url, version) values(7, 'tech', 'tech news', null, 0);
insert into news(id, name, description, icon_url, version) values(8, 'liverpool', 'liverpool news', '/news/liverpool.jpg', 0);
insert into news(id, name, description, icon_url, version) values(9, 'java', 'java news', '/default.jpg', 0);
insert into news(id, name, description, icon_url, version) values(10, 'apple', 'apple news', '/default.jpg', 0);


insert into app_user(id, email, password, version) values(1, 'test@email.com', '{bcrypt}$2a$10$zjKx/rWWL4pndIBIfwgQdOAfTbT0rcn/VFxBV21qVWN.Wkpi4OPHm', 0);


insert into user_news(user_id, news_id) values(1, 1);
insert into user_news(user_id, news_id) values(1, 2);
insert into user_news(user_id, news_id) values(1, 3);
insert into user_news(user_id, news_id) values(1, 4);
insert into user_news(user_id, news_id) values(1, 5);

-------------------------------------------------------------------------------------------------------------

Login service:
POST http://localhost:8080/login
Body:
{
    "email":"test@email.com",
    "password":"Passw0rd"

}
Response:
{
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0QGVtYWlsLmNvbSIsImV4cCI6MTYwMDUzMDU0MCwicm9sZSI6IlVTRVIifQ.tGUj6O67GOXH6UcBiPD7OiSJgk-MJdm8Ut0sFZKI0LA"
}

-------------------------------------------------------------------------------------------------------------

Get all news service:
GET http://localhost:8080/news?pageSize=10&pageNum=1
Header: Athentication: Bearer <token>
Response:
{
    "totalItems": 10,
    "news": [
        {
            "name": "sport",
            "description": "the sports news",
            "iconUrl": "/news/sports.jpg"
        },
        {
            "name": "politics",
            "description": "the politics news",
            "iconUrl": "/news/politics.jpg"
        }
    ],
    "totalPages": 5,
    "currentPage": 1
}

-------------------------------------------------------------------------------------------------------------

Get user news service:
GET http://localhost:8080/user/news
Header: Athentication: Bearer <token>
Response:
{

    "news": [
        {
            "name": "sport",
            "description": "the sports news",
            "iconUrl": "/news/sports.jpg"
        },
        {
            "name": "politics",
            "description": "the politics news",
            "iconUrl": "/news/politics.jpg"
        }
    ]
}

-------------------------------------------------------------------------------------------------------------

Subscribe:
POST http://localhost:8080/news/subscribe
Header: Athentication: Bearer <token>

Body:
{
    "requiredNews": [
        {
            "name": "apple",
            "description": "apple news",
            "iconUrl": "/default.jpg"
        },
        {
            "name": "java",
            "description": "java news",
            "iconUrl": "/default.jpg"
        }
    ]
}
Response:
{
    "newsAdded": [
    {
            "name": "apple",
            "description": "apple news",
            "iconUrl": "/default.jpg"
   }
   ]
}

-------------------------------------------------------------------------------------------------------------

Unubscribe:
DELETE http://localhost:8080/news/unsubscribe
Header: Athentication: Bearer <token>

Body:
{
    "requiredNews": [
        {
            "name": "apple",
            "description": "apple news",
            "iconUrl": "/default.jpg"
        },
        {
            "name": "java",
            "description": "java news",
            "iconUrl": "/default.jpg"
        }
    ]
}

Response:
{
    "newsRemoved": [
    {
            "name": "apple",
            "description": "apple news",
            "iconUrl": "/default.jpg"
   }
   ]
}
